#include <SPI.h>
#include <SdFat.h>
#include <SdFatUtil.h> 
#include <SFEMP3Shield.h>

#include <Wire.h>
#include "gyro_accel.h"\\
// Defining constants
#define dt 20                       // time difference in milli seconds
#define rad2degree 57.3              // Radian to degree conversion
#define Filter_gain 0.95             // e.g.  angle = angle_gyro*Filter_gain + angle_accel*(1-Filter_gain)
// *********************************************************************
//    Global Variables
// *********************************************************************
unsigned long t=0; // Time Variables
float angle_x_gyro=0,angle_y_gyro=0,angle_z_gyro=0,angle_x_accel=0,angle_y_accel=0,angle_z_accel=0,angle_x=0,angle_y=0,angle_z=0,velocity;

SdFat sd;
SFEMP3Shield MP3player;


// Define constants for using echo and trig pin
const int echopin = 3;
const int trigpin = 4;
const int vibe = 5;
int tempPin = A0;
long duration;
long in;
float temper;
byte result;


void setup() {
  

  Serial.begin(9600);
  
  Wire.begin();
  MPU6050_ResetWake();
  MPU6050_SetGains(0,1);// Setting the lows scale
  MPU6050_SetDLPF(0); // Setting the DLPF to inf Bandwidth for calibration
  MPU6050_OffsetCal();
  MPU6050_SetDLPF(6); // Setting the DLPF to lowest Bandwidth
  
   t=millis();
  
  //start the shield
   result = sd.begin(SD_SEL, SPI_HALF_SPEED);
  
  //boot up the MP3 Player Shield
  result = MP3player.begin();
  //check result, see readme for error codes.
  if(result != 0) {
    Serial.print("Error code: ");
    Serial.print(result);
    Serial.println(" when trying to start MP3 player");
    }

  pinMode(echopin, INPUT);
  pinMode(trigpin, OUTPUT);
  pinMode(vibe, OUTPUT);
}

//do something else now
void loop() {
  
  t=millis(); 
  
  MPU6050_ReadData();
 
  angle_x_gyro = (gyro_x_scalled*((float)dt/1000)+angle_x);
  angle_y_gyro = (gyro_y_scalled*((float)dt/1000)+angle_y);
  angle_z_gyro = (gyro_z_scalled*((float)dt/1000)+angle_z);

  angle_z_accel = atan(accel_z_scalled/(sqrt(accel_y_scalled*accel_y_scalled+accel_x_scalled*accel_x_scalled)))*(float)rad2degree;
  angle_y_accel = -atan(accel_y_scalled/(sqrt(accel_y_scalled*accel_y_scalled+accel_z_scalled*accel_z_scalled)))*(float)rad2degree;
  angle_x_accel = atan(accel_x_scalled/(sqrt(accel_x_scalled*accel_x_scalled+accel_z_scalled*accel_z_scalled)))*(float)rad2degree;

  angle_x = Filter_gain*angle_x_gyro+(1-Filter_gain)*angle_x_accel;
  angle_y = Filter_gain*angle_y_gyro+(1-Filter_gain)*angle_y_accel;
  angle_z = Filter_gain*angle_z_gyro+(1-Filter_gain)*angle_z_accel;

  float velocity_x, velocity_y, velocity_z;

  velocity_z += (accel_z_scalled)*dt;
  velocity_y += (accel_y_scalled)*dt;
  velocity_x += accel_x_scalled*dt;


  velocity= sqrt( velocity_x*velocity_x + velocity_y*velocity_y + velocity_z*velocity_z)/1000;




  


  Serial.print("\t");
  while((millis()-t) < dt){ // Making sure the cycle time is equal to dt
  // Do nothing
  }
  
    // put your main code here, to run repeatedly:

  temper = (getVoltage(tempPin) - 0.5) * 100;
  //Serial.print(temp);
  //Serial.println(" C");

  digitalWrite(trigpin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigpin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigpin, LOW);
  
  duration = pulseIn(echopin, HIGH);

  in = microsecondsToInches(duration, temper); 

  Serial.print(in);
  Serial.println(" in");
  delay(200);

	//three alerts
	int distances[3] = [20, 50, 80];
	//define audio tracks to play..
	int tracks[3] = [1, 2, 3];

	//closest one, vibrate
	if( in > distances[0] && in > distances[1]){

		//vibrate and send message
		MP3player.playTrack(track[0]);

	}

	if( in > distances[1] && in > distances[2]){

		//send message at intermediate range
		MP3player.playTrack(track[1]);

	}

	if( in > distances[2] ){

		//object is relatively far
		MP3player.playTrack(track[2]);

	}


	if(in < 15){
	  
		if (abs(angle_y)>44){Serial.print("left");Serial.print("\t");}
		if(abs(angle_y)<42){Serial.print("right");Serial.print("\t");}
		if(abs(angle_y)>42&&abs(angle_y)<44){Serial.print("middle");Serial.print("\t");}

		MP3player.playTrack(1);
		//make vibrator vibrate. testing without
		digitalWrite(vibe, HIGH);
		delay(200);
		digitalWrite(vibe,LOW);
	}
	else
	{
		MP3player.stopTrack();
	}
}

float getVoltage(int pin) { 
  return (analogRead(pin) * .004882814); //Converting from 0 to 1024 to 0 to 5v 
}

long microsecondsToInches(long microseconds, long temper) {
  return ((microseconds*(331.3 + (0.606 * temper)))*0.000019685);    //Multiplying the speed of sound through a certain temperature of air by the                                                   
                                                                  //length of time it takes to reach the object and back, divided by two 
}
